from django.views.generic import DetailView, ListView, CreateView
from .models import Company, Car
from .forms import CarForm
from apps.order.models import RequestCar
from apps.track.models import TrackOffer


class CompanyDetailView(DetailView):
    model = Company

    def get_object(self, queryset=None):
        if hasattr(self.request.user, 'company'):
            return self.request.user.company
        return None


class CarListView(ListView):
    model = Car
    template_name = 'company/car_list.html'

    def get_queryset(self):
        if hasattr(self.request.user, 'company'):
            return self.request.user.company.car_set.all()
        return None


class CarCreateView(CreateView):
    model = Car
    form_class = CarForm
    template_name = 'company/car_create.html'
    success_url = '/company/cars/'

    def form_valid(self, form):
        form.instance.company = self.request.user.company
        return super(CarCreateView, self).form_valid(form)


class RequsetCarListView(ListView):
    model = RequestCar
    template_name = 'company/requestcar_list.html'


class TrackOfferListView(ListView):
    model = TrackOffer
    template_name = 'company/trackoffer_list.html'
