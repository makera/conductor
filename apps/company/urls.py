from django.urls import path
from .views import CompanyDetailView, CarListView, CarCreateView, RequsetCarListView, TrackOfferListView

urlpatterns = [
    path('', CompanyDetailView.as_view()),
    path('cars/', CarListView.as_view()),
    path('cars/create/', CarCreateView.as_view()),
    path('requests/', RequsetCarListView.as_view()),
    path('routes/', TrackOfferListView.as_view()),
]
