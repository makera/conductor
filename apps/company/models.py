from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from sorl.thumbnail.fields import ImageField
from apps.utils.models import DateAddDateChangeMixin

car_types = (
    (1, 'Автобус'),
    (2, 'Троллейбус'),
    (3, 'Трамвай'),
    (4, 'Маршрутное транспортное средство'),
)


class Company(models.Model, DateAddDateChangeMixin):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название', max_length=255)

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'

    def __str__(self):
        return self.name


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Company.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_company(sender, instance, **kwargs):
    if hasattr(instance, 'company'):
        instance.company.save()


class Car(models.Model, DateAddDateChangeMixin):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название компании', max_length=50)
    reg_num = models.CharField(verbose_name='Регистрационный номер', max_length=10)
    verify = models.BooleanField(verbose_name='Верифицировано', default=False)

    class Meta:
        verbose_name = 'Транспортное средство'
        verbose_name_plural = 'Транспортные средства'

    def __str__(self):
        return self.name


class CarImage(models.Model, DateAddDateChangeMixin):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    image = ImageField(verbose_name='Картинка', upload_to='uploads/cars/')
