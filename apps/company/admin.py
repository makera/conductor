from django.contrib import admin
from .models import Car, Company, CarImage


class CarImageTabularInline(admin.TabularInline):
    model = CarImage
    extra = 3


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    pass


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    pass
