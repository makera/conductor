from django import forms
from .models import Car


class CarForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = ['reg_num', 'name']
        widgets = {
            'reg_num': forms.TextInput(attrs={'class': 'mdl-textfield__input'}),
            'name': forms.TextInput(attrs={'class': 'mdl-textfield__input'}),
        }
