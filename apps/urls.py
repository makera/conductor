from django.urls import path, include
from django.views.generic import TemplateView

urlpatterns = [
    path('', include('apps.passenger.urls')),
    path('rent/', include('apps.order.urls')),
    path('company/', include('apps.company.urls')),
]
