from django import forms


class TrackForm(forms.Form):
    departure = forms.CharField(widget=forms.TextInput(attrs={'class': 'mdl-textfield__input'}))
    destination = forms.CharField(widget=forms.TextInput(attrs={'class': 'mdl-textfield__input'}))
