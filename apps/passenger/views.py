from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from apps.utils.track import get_track
from .forms import TrackForm
from apps.track.models import TrackOffer


class SearchTrackView(TemplateView):
    form_class = TrackForm
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form_kwargs = {'data': self.request.POST}
        form = self.form_class(**form_kwargs)
        context['form'] = form
        if self.request.POST:
            try:
                context['track'] = get_track(self.request.POST['departure'], self.request.POST['destination'])
                context['map'] = True
            except:
                context['track'] = 'Маршрутов не найдено'
        else:
            context['track'] = ''
        return context

    def post(self, request, **kwargs):
        if 'request' in request.POST:
            TrackOffer.objects.create(passenger=request.user, departure=request.POST['departure'],
                                      destination=request.POST['destination'])
            return HttpResponseRedirect('/')
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
