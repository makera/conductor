from django.urls import path
from .views import SearchTrackView

urlpatterns = [
    path('', SearchTrackView.as_view()),
]
