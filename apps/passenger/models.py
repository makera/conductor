from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.utils.models import DateAddDateChangeMixin


class Passenger(models.Model, DateAddDateChangeMixin):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    # ava =

    class Meta:
        verbose_name = 'Пассажир'
        verbose_name_plural = 'Пассажиры'

    def __str__(self):
        return ' '.join([self.user.first_name, self.user.last_name])


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Passenger.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_passenger(sender, instance, **kwargs):
    if hasattr(instance, 'passenger'):
        instance.passenger.save()
