from django.views.generic import ListView, CreateView
from .models import RequestCar
from .forms import RequestCarForm


class RequestCarCreateView(CreateView):
    model = RequestCar
    template_name = 'order/requestcar_create.html'
    form_class = RequestCarForm
    success_url = '/rent/success'

    def form_valid(self, form):
        form.instance.passenger = self.request.user
        return super(RequestCarCreateView, self).form_valid(form)
