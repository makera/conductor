from django.urls import path
from .views import RequestCarCreateView


urlpatterns = [
    path('', RequestCarCreateView.as_view()),
]