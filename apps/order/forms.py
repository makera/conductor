from django import forms
from .models import RequestCar


class RequestCarForm(forms.ModelForm):
    class Meta:
        model = RequestCar
        fields = ['capacity', 'date_start', 'date_end']
        widgets = {
            'capacity': forms.NumberInput(attrs={'class': 'mdl-textfield__input'}),
            'date_start': forms.DateInput(attrs={'class': 'mdl-textfield__input'}),
            'date_end': forms.DateInput(attrs={'class': 'mdl-textfield__input'}),
        }
