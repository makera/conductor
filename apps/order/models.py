import datetime
from django.db import models
from django.contrib.auth.models import User
from apps.company.models import Car
from apps.passenger.models import Passenger
from apps.utils.models import DateAddDateChangeMixin


class OrderCar(models.Model, DateAddDateChangeMixin):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    passenger = models.ForeignKey(User, on_delete=models.CASCADE)
    price = models.DecimalField(verbose_name='Стоимость', default=0, max_digits=12, decimal_places=2)
    date_start = models.DateTimeField(verbose_name='Дата начала', default=datetime.datetime.now)
    date_end = models.DateTimeField(verbose_name='Дата окончания', default=datetime.datetime.now)

    class Meta:
        verbose_name = 'Заказ авто'
        verbose_name_plural = 'Заказы авто'

    def __str__(self):
        return ' -> '.join([self.passenger.__str__(), self.car])


class RequestCar(models.Model, DateAddDateChangeMixin):
    passenger = models.ForeignKey(User, on_delete=models.CASCADE)
    capacity = models.PositiveIntegerField(verbose_name='Вместимость человек', default=1)
    date_start = models.DateTimeField(verbose_name='Дата начала', default=datetime.datetime.now)
    date_end = models.DateTimeField(verbose_name='Дата окончания', default=datetime.datetime.now)

    class Meta:
        verbose_name = 'Запрос авто'
        verbose_name_plural = 'Запросы авто'

    def __str__(self):
        return ' -> '.join([self.passenger.__str__(), self.date_start.__str__()])


class OfferCar(models.Model, DateAddDateChangeMixin):
    request = models.ForeignKey(RequestCar, on_delete=models.CASCADE)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    price = models.DecimalField(verbose_name='Стоимость', default=0, max_digits=12, decimal_places=2)

    class Meta:
        verbose_name = 'Предложение авто'
        verbose_name_plural = 'Предложения авто'

    def __str__(self):
        return ' -> '.join([self.request, self.car])
