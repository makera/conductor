from django.contrib import admin
from .models import OrderCar, OfferCar, RequestCar


@admin.register(OrderCar)
class OrderCarAdmin(admin.ModelAdmin):
    pass


@admin.register(OfferCar)
class OfferCarAdmin(admin.ModelAdmin):
    pass


@admin.register(RequestCar)
class RequestCarAdmin(admin.ModelAdmin):
    pass
