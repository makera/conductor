from django.contrib import admin
from .models import Track, TrackCar, TrackOffer


@admin.register(Track)
class TrackAdmin(admin.ModelAdmin):
    pass


@admin.register(TrackCar)
class TrackCarAdmin(admin.ModelAdmin):
    pass


@admin.register(TrackOffer)
class TrackOfferAdmin(admin.ModelAdmin):
    pass
