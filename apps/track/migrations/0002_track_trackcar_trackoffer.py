# Generated by Django 2.2.3 on 2019-07-27 12:27

import apps.utils.models
import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('company', '0002_auto_20190727_2127'),
        ('passenger', '0002_passenger'),
        ('track', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название')),
                ('departure', models.CharField(max_length=200, verbose_name='Пункт отрпавления')),
                ('destination', models.CharField(max_length=200, verbose_name='Пункт назначения')),
                ('date_start', models.DateTimeField(default=datetime.datetime.now, verbose_name='Время отправления')),
                ('date_end', models.DateTimeField(default=datetime.datetime.now, verbose_name='Время прибытия')),
            ],
            options={
                'verbose_name': 'Маршрут',
                'verbose_name_plural': 'Маршруты',
            },
            bases=(models.Model, apps.utils.models.DateAddDateChangeMixin),
        ),
        migrations.CreateModel(
            name='TrackOffer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('departure', models.CharField(max_length=200, verbose_name='Пункт отрпавления')),
                ('destination', models.CharField(max_length=200, verbose_name='Пункт назначения')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Дата маршрута')),
                ('time_range', models.CharField(max_length=50, verbose_name='Временной диапазон')),
                ('count', models.PositiveIntegerField(default=1, verbose_name='Количество человек')),
                ('passenger', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='passenger.Passenger')),
            ],
            bases=(models.Model, apps.utils.models.DateAddDateChangeMixin),
        ),
        migrations.CreateModel(
            name='TrackCar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Car')),
                ('track', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='track.Track')),
            ],
            bases=(models.Model, apps.utils.models.DateAddDateChangeMixin),
        ),
    ]
