import datetime
from django.db import models
from apps.utils.models import DateAddDateChangeMixin
from apps.company.models import Car
from apps.passenger.models import User


class Track(models.Model, DateAddDateChangeMixin):
    name = models.CharField(verbose_name='Название', max_length=200)
    departure = models.CharField(verbose_name='Пункт отрпавления', max_length=200)
    destination = models.CharField(verbose_name='Пункт назначения', max_length=200)
    date_start = models.DateTimeField(verbose_name='Время отправления', default=datetime.datetime.now)
    date_end = models.DateTimeField(verbose_name='Время прибытия', default=datetime.datetime.now)

    class Meta:
        verbose_name = 'Маршрут'
        verbose_name_plural = 'Маршруты'

    def __str__(self):
        return self.name


class TrackCar(models.Model, DateAddDateChangeMixin):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Авто на маршруте'
        verbose_name_plural = 'Авто на маршруте'

    def __str__(self):
        return ' -> '.join([self.track, self.car])


class TrackOffer(models.Model, DateAddDateChangeMixin):
    passenger = models.ForeignKey(User, on_delete=models.CASCADE)
    departure = models.CharField(verbose_name='Пункт отрпавления', max_length=200)
    destination = models.CharField(verbose_name='Пункт назначения', max_length=200)
    date = models.DateField(verbose_name='Дата маршрута', default=datetime.date.today)
    time_range = models.CharField(verbose_name='Временной диапазон', max_length=50, blank=True)
    count = models.PositiveIntegerField(verbose_name='Количество человек', default=1)

    class Meta:
        verbose_name = 'Заказ маршрута'
        verbose_name_plural = 'Заказы маршрута'

    def __str__(self):
        return ' -> '.join([self.passenger.username, self.departure, self.destination])
