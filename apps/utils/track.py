import math
from collections import deque

map = {'A': ['B', 'F', 'K'],
       'B': ['A', 'D', 'C'],
       'C': ['B', 'E'],
       'D': ['B'],
       'E': ['C'],
       'F': ['A', 'G'],
       'G': ['F', 'H'],
       'H': ['G'],
       'K': ['A', 'L', 'O'],
       'L': ['M', 'K'],
       'M': ['N', 'L'],
       'N': ['M'],
       'O': ['K']
       }

exist_tracks = ['ABCE', 'AFGH', 'AKLM', 'BD', 'OKLMN',
                'ECBA', 'HGFA', 'MLKA', 'DB', 'NMLKO']

times = [{'BD': ['10:00', '11:00']},
         {'DB': ['12:00', '13:00']},
         {'ABCE': ['08:00', '08:30', '09:00', '09:30']},
         {'ECBA': ['15:00', '15:30', '16:00', '16:30']},
         {'AFGH': ['14:20', '14:50', '16:10', '17:20']},
         {'HGFA': ['18:00', '19:10', '19:50', '20:20']},
         {'AKLM': ['07:30', '08:30', '09:00', '09:30']},
         {'MLKA': ['16:00', '16:30', '17:00', '17:30']},
         {'OKLMN': ['11:00', '12:00', '13:00', '14:00', '15:00']},
         {'NMLKO': ['16:00', '17:00', '18:00', '19:00', '20:00']}
         ]


def paths(graph, start, finish):
    queue = deque([(start, [start])])
    while queue:
        (vertex, path) = queue.pop()
        for next in set(graph[vertex]) - set(path):
            if next == finish:
                yield path + [next]
            else:
                queue.appendleft((next, path + [next]))


def shortest_path(graph, start, finish):
    try:
        return next(paths(graph, start, finish))
    except StopIteration:
        print('Невозможно построить маршрут')
        return None


def get_match(exist_tracks_work, test_track):
    for unit in exist_tracks_work:
        if test_track.startswith(unit):
            return unit


def get_time(start, finish):
    print(start, finish)
    for time in times:
        track = list(time.keys())[0]
        # print(track) # ==========================================
        time_1 = track.find(start)
        time_2 = track.find(finish)
        # print(time_1, time_2) # ====================================
        if time_1 != -1 and time_2 != -1 and time_1 < time_2:
            return time[track][time_1], time[track][time_2]


def get_track(start, finish):
    direct_track = ''.join(shortest_path(map, start, finish))
    # print(direct_track) #================================================
    track = []
    test_track = direct_track
    exist_tracks_work = exist_tracks[:]
    exist_tracks_del_1 = exist_tracks[:]
    exist_tracks_del_2 = exist_tracks[:]

    exist_tracks_lens = [len(x) for x in exist_tracks_work]
    exist_tracks_lens_min = min(exist_tracks_lens)
    exist_tracks_lens_max = max(exist_tracks_lens)
    # print(exist_tracks_lens, exist_tracks_lens_max, exist_tracks_lens_min)

    while exist_tracks_del_1:
        for j in range(len(exist_tracks_del_1)):
            exist_tracks_del_1[j] = '' if (len(exist_tracks_del_1[j]) < 2) else exist_tracks_del_1[j]
        while '' in exist_tracks_del_1:
            exist_tracks_del_1.remove('')
        exist_tracks_del_1 = [x[0:-1] for x in exist_tracks_del_1]
        exist_tracks_work.extend(exist_tracks_del_1)

    while exist_tracks_del_2:
        for j in range(len(exist_tracks_del_2)):
            exist_tracks_del_2[j] = '' if (len(exist_tracks_del_2[j]) < 2) else exist_tracks_del_2[j]
        while '' in exist_tracks_del_2:
            exist_tracks_del_2.remove('')
        exist_tracks_del_2 = [x[1:] for x in exist_tracks_del_2]
        exist_tracks_work.extend(exist_tracks_del_2)

    for j in range(len(exist_tracks_work)):
        exist_tracks_work[j] = '' if (len(exist_tracks_work[j]) < 2) else exist_tracks_work[j]
    while '' in exist_tracks_work:
        exist_tracks_work.remove('')

    # print('exist_tracks_work', exist_tracks_work) #=============================================

    for i in range(int(math.ceil(exist_tracks_lens_max) / 2) + 1):
        unit = get_match(exist_tracks_work, test_track)
        # print('unit', unit) #====================================
        if unit:
            track.append(unit)
            test_track = test_track.replace(unit[:-1], '')

    # print()
    # print(track) #==========================================================
    # print('test_track', test_track)
    # print('exist_tracks_work', exist_tracks_work)
    # print('direct_track', direct_track)

    times = [track.__str__()]
    for unit in track:
        times.append(get_time(unit[0], unit[-1]).__str__())
    return '<br>'.join(times)
