from django.db import models


class DateAddDateChangeMixin(object):
    date_create = models.DateTimeField(verbose_name='Дата добавления', auto_now_add=True)
    date_change = models.DateTimeField(verbose_name='Дата изменения', auto_now=True)
